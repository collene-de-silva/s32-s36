const Course = require('../models/Course');

module.exports.addCourse = (reqBody) => {

	let newCourse = new Course ({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	})

	return newCourse.save().then((course, error) => {
		if (error) {
			return false
		} else {
			return course
		}
	})

} 

// Other solution (di pa tapos)
// module.exports.addCourse = (reqBody, userData) => {

// 	return Course.findById(userData.userId).then(result => {
// 		id (userData.isAdmin === false) {
// 			return
// 		}
// 	})

// 	return newCourse.save().then((course, error) => {
// 		if (error) {
// 			return false
// 		} else {
// 			return course
// 		}
// 	})

// } 


//Retrieve all Courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result
	})
}

//Retrieve active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	})
}

//Retrieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

//Update a course 
module.exports.updateCourse = (data) => {
	console.log(data) 

	return Course.findById(data.courseId).then((result, error) => {
		console.log(result)

		if (data.isAdmin) {
			result.name = data.updatedCourse.name
			result.description = data.updatedCourse.description
			result.price = data.updatedCourse.price

			console.log(result)

			return result.save().then((updatedCourse, error) => {
				if (error) {
					return false
				} else {
					return updatedCourse
				}
			})
		} else {
			return "Not Admin"
		}
	})
}

//Archive a course
module.exports.archiveCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		result.isActive = false
		
		return result.save().then((archivedCourse, error) => {
			if (error) {
				return false
			} else {
				return archivedCourse
			}
		})
	})
}