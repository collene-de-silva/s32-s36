const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers');
const auth = require('../auth');

//Route for checkign if email exists
router.post('/checkEmail', (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});


//Route for User Registration 
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

//Routes for user authentication
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Create a /details route that will accept the user’s Id to retrieve the details of a user.
//the auth.verify will act as a middleware to ensure that the user 
router.get('/details', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	console.log(userData)


	userController.getProfile({userId: userData.id}).then(
		resultFromController => res.send(resultFromController))
});

//Enrolling user
router.post('/enroll', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin === false) {
		let data = {
		userId: userData.id,
		courseId: req.body.courseId
		}

		userController.enroll(data).then(resultFromController => res.send(resultFromController))

	} else {
		res.send("Not Authorized")
	}
})

// router.get('/consoler', auth.verify, (req, res) => {
// 	console.log(auth.decode(req.headers.authorization).id)
// })

module.exports = router;

