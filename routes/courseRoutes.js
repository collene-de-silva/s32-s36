const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseControllers');
const auth = require('../auth')

//Route for creating a course
router.post('/', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin) {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Not Authorized")
	}
})

//Activity Solution 2
/*router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    courseController.addCourse(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})*/



//Route for retrieving all courses 
router.get('/all', auth.verify, (req,res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
});


//Mini-Activity 
/*
	Create a route and controller for retrieving all active courses. No need to logged in.

	getAllActive function 
	'/'
*/
router.get('/', (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
})


//Route for retrieving a specific course 
//Url: localhost:4000/courses/343241341431432
router.get('/:courseId', (req, res) => {

	//_id = courseId. yung _ is yung model  
	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

//Route for updating a course (admin only)
router.put('/:courseId', auth.verify, (req, res) => {
	const data = {
		courseId: req.params.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		updatedCourse: req.body
	}

	console.log(req.headers)

	courseController.updateCourse(data).then(resultFromController => res.send(resultFromController))
})


//isActive === false
router.put('/:courseId/archive', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin) {
		courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Not Authorized")
	}
})



module.exports = router;
 